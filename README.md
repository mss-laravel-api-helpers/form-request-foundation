# REST API FormRequest Foundation

### About

This package is for use with Laravel 5.4+ REST APIs. The intention of this package is to provide a foundation for extendable form requests
outside of what the Laravel framework currently provides. This extensibility includes authorizing form requests based on model policies and
building model validation rules.

### Installation

Add the following to your `composer.json`:
```
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/mss-laravel-api-helpers/form-request-foundation.git"
    }
]
```

Require via `composer`:
```
composer require gila-api/form-request-foundation
```

**Current Version:**
```
composer require gila-api/form-request-foundation v3.0.0
```

### Usage

**Step One: Create a FormRequest**

Create a new request in your project and extend `Gila\LaravelApiHelpers\FormRequestFoundation\AbstractApiRequestValidator`:

```php
use Gila\LaravelApiHelpers\FormRequestFoundation\AbstractApiRequestValidator;
use Gila\LaravelApiHelpers\FormRequestFoundation\Contracts\BaseRequestContract;

class IndexRequest extends AbstractApiRequestValidator implements BaseRequestContract
{
    ...
}
```

**Step Two: Implement Policy**

In order to authorize form requests, we need to implement a [model policy](https://laravel.com/docs/5.7/authorization#creating-policies).
Your policy should extend `Gila\LaravelApiHelpers\FormRequestFoundation\AbstractBasePolicy`. Your model policy will need to contain methods
that facilitate authorization for each request type, even if it just returns `true`.

Implement the policy in the form request:
```php
class IndexRequest extends AbstractApiRequestValidator implements BaseRequestContract
{
    public function getPolicyAction(): string
    {
        return MyModelPolicy::ACTION_INDEX;
    }
    
    public function getPolicyActor()
    {
        return MyModel::class;
    }
}
```

**Step Three: Use the Request!**

Now you are ready to use this request in a controller. Pass this form request as the `Request` parameter of your controller method.

### Authorizing Requests

To authorize a request, you should use the `Gila\LaravelApiHelpers\FormRequestFoundation\Traits\Authorizes` trait in your form request. This
trait will attempt to authorize the request based on the policy action and policy actor, ultimately directing authorization to your model
policy.

```php
use Gila\LaravelApiHelpers\FormRequestFoundation\Traits\Authorizes;

class IndexRequest extends AbstractApiRequestValidator implements BaseRequestContract
{
    use Authorizes;
    
    ...
}
```

In order to authorize on an actual model included in the request, you should return the model in the route path from the `getPolicyActor`
method:

```php
public function getPolicyActor()
{
    return $this->route('model');
}
```

This will pass the model in the route to your model policy.

### Deferring Authorization on Requests

If you wish to defer authorization on a request, you should use the `Gila\LaravelApiHelpers\FormRequestFoundation\Traits\DefersAuthorization`
trait. This trait simple returns `true` from authorization.

```php
use Gila\LaravelApiHelpers\FormRequestFoundation\Traits\DefersAuthorization;

class IndexRequest extends AbstractApiRequestValidator implements BaseRequestContract
{
    use DefersAuthorization;
    
    ...
}
```

### Validating Model Rules

Form requests are capable of validating rules from models on an incoming request. This is especially useful for automatic validation on CRUD
requests.

```php
use Gila\LaravelApiHelpers\FormRequestFoundation\Contracts\ValidationRulesContract;

class IndexRequest extends AbstractApiRequestValidator implements BaseRequestContract
{
    public function rules(MyModel $model): array
    {
        return $model->buildValidationRules(ValidationRulesContract::VALIDATE_CREATE);
    }
}
```

In order to validate rules on a model, your model must implement `ValidationRulesContract`, define rules, and implement the trait
`Gila\LaravelApiHelpers\FormRequestFoundation\Models\Traits\BuildsValidationRules`.

```php
use Gila\LaravelApiHelpers\FormRequestFoundation\Models\Traits\BuildsValidationRules;
use Gila\LaravelApiHelpers\FormRequestFoundation\Contracts\ValidationRulesContract;

class MyModel extends Model implements ValidationRulesContract
{
    use BuildsValidationRules;
    
    public function rules(): array
    {
        return [
            ValidationRulesContract::VALIDATE_BASE => [
                // Place base validation rules here
                // These rules are applied to every request
            ],
            ValidationRulesContract::VALIDATE_CREATE => [
                // Place create validation rules here
                // These rules are applied to store requests
            ],
            ValidationRulesContract::VALIDATE_UPDATE => [
                // Place update validation rules here
                // These rules are applied to update requests
            ]
        ];
    }
}

```

When defining validation rules, you should create the rules for a column as an `array` of rules:

```php
public function rules(): array
{
    return [
        ValidationRulesContract::VALIDATE_BASE => [
            'column' => ['string', 'max:120']
        ]
    ];
}
```

You may also use the `|` pipe operator instead of an array:

```php
public function rules(): array
{
    return [
        ValidationRulesContract::VALIDATE_BASE => [
            'column' => ['string|max:120']
        ]
    ];
}
```