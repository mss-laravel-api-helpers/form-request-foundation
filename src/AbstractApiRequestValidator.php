<?php
/**
 * The parent class for form request validation
 */

namespace Gila\LaravelApiHelpers\FormRequestFoundation;

use Gila\LaravelApiHelpers\ExceptionFoundation\Exceptions\FormValidationException;
use Gila\LaravelApiHelpers\FormRequestFoundation\Contracts\BaseRequestContract;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AbstractApiRequestValidator
 * @package Gila\LaravelApiHelpers\FormRequestFoundation
 */
abstract class AbstractApiRequestValidator extends FormRequest implements BaseRequestContract
{
    /**
     * @const string
     */
    protected const DATATYPE_JSON = 'json';

    /**
     * @const string
     */
    protected const DATATYPE_FORM = 'form';

    /**
     * The data type for request input and expectations
     *
     * @var string
     */
    protected $dataType = self::DATATYPE_JSON;

    /**
     * The fields that are included in validationData. Empty array for all fields.
     *
     * @var array
     */
    protected $validationData = [];

    /**
     * @return bool
     */
    public function expectsJson(): bool
    {
        return ($this->dataType === 'json');
    }

    /**
     * Validates the incoming request
     *
     * @param Validator $validator
     */
    public function withValidator(Validator $validator): void
    {
        $validator->after(function () use ($validator): void {
            if ($validator->errors()->isNotEmpty()) {
                throw new FormValidationException($validator->errors()->getMessages());
            }
        });
    }

    /**
     * Sets the data type for the request (either 'json' or 'form')
     *
     * It is recommended to use 'json' for REST APIs
     *
     * @param string $dataType
     */
    protected function setDataType(string $dataType): void
    {
        if ($dataType !== self::DATATYPE_FORM && $dataType !== self::DATATYPE_JSON) {
            $dataType = self::DATATYPE_FORM;
        }

        $this->dataType = $dataType;
    }

    /**
     * Get the request data for validation
     *
     * @return array
     */
    public function validationData(): array
    {
        if (!empty($this->validationData)) {
            return ($this->expectsJson()) ? $this->filterJsonValidationData() : $this->only($this->validationData);
        }

        return ($this->expectsJson()) ? $this->json()->all() : $this->all();
    }

    /**
     * Sets specific keys from request to use for validation data
     *
     * @param array $keysToValidate
     */
    protected function setValidationData(array $keysToValidate): void
    {
        $this->validationData = $keysToValidate;
    }

    /**
     * Filters json to only get keys in validation data
     *
     * @return array
     */
    protected function filterJsonValidationData(): array
    {
        $data = [];
        foreach ($this->validationData as $key) {
            $data[$key] = $this->json($key);
        }

        return $data;
    }
}
