<?php
/**
 * Parent class for model policies to be used in FormRequest validation
 */

namespace Gila\LaravelApiHelpers\FormRequestFoundation;

/**
 * Class AbstractBasePolicy
 * @package Gila\LaravelApiHelpers\FormRequestFoundation
 */
abstract class AbstractBasePolicy
{
    /**
     * The actions
     *
     * @const string
     */
    const ACTION_INDEX = 'all';
    const ACTION_VIEW = 'show';
    const ACTION_STORE = 'store';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'destroy';
}
