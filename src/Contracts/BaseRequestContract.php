<?php
/**
 * Contract implemented on FormRequests
 */

namespace Gila\LaravelApiHelpers\FormRequestFoundation\Contracts;

/**
 * Interface BaseRequestContract
 * @package Gila\LaravelApiHelpers\FormRequestFoundation\Contracts
 */
interface BaseRequestContract
{
    /**
     * Gets the model class or model in path for the request
     *
     * @return mixed
     */
    public function getPolicyActor();

    /**
     * Gets the action of the request (from AbstractBasePolicy)
     *
     * @return string
     */
    public function getPolicyAction(): string;
}
