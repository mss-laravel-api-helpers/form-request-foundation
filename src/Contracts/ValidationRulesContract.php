<?php
/**
 * Contract to be implemented on models that have validation rules
 */

namespace Gila\LaravelApiHelpers\FormRequestFoundation\Contracts;

/**
 * Interface ValidationRulesContract
 * @package Gila\LaravelApiHelpers\FormRequestFoundation\Contracts
 */
interface ValidationRulesContract
{
    /**
     * Methods that use validation
     *
     * @const string
     */
    const VALIDATE_BASE = 'base';
    const VALIDATE_CREATE = 'create';
    const VALIDATE_UPDATE = 'update';

    /**
     * Validation qualifiers
     *
     * @const string|array
     */
    const VALIDATION_REQUIRED = 'required';
    const VALIDATION_BAIL = 'bail';
    const VALIDATION_SQL_TIMESTAMP = 'date_format:Y-m-d H:i:s';
    const VALIDATION_SLUG = ['string', 'alpha_dash'];

    /**
     * Validation message templates
     *
     * @const string
     */
    const VALIDATION_MESSAGE_REQUIRED = '%s is required';
    const VALIDATION_MESSAGE_INTEGER = '%s must be an integer';
    const VALIDATION_MESSAGE_STRING = '%s must be a string';
    const VALIDATION_MESSAGE_DATE = '%s must be a date';
    const VALIDATION_MESSAGE_BOOL = '%s must be a boolean';

    /**
     * Provides array of validation rules
     *
     * @return array
     */
    public function rules(): array;
}
