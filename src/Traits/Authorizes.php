<?php
/**
 * Trait to provide authorization by policyAction and policyActor on a request
 */

namespace Gila\LaravelApiHelpers\FormRequestFoundation\Traits;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

trait Authorizes
{
    use AuthorizesRequests {
        AuthorizesRequests::authorize as authorizeRequest;
    }

    /**
     * Authorizes request from form request
     *
     * @return bool
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function authorize(): bool
    {
        $this->authorizeRequest($this->getPolicyAction(), $this->getPolicyActor());
        return true;
    }
}