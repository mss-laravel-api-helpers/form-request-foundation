<?php
/**
 * Trait to defer authorization on a form request
 */

namespace Gila\LaravelApiHelpers\FormRequestFoundation\Traits;

/**
 * Trait DefersAuthorization
 * @package Gila\LaravelApiHelpers\FormRequestFoundation\Traits
 */
trait DefersAuthorization
{
    /**
     * Returns 'true' to allow auth to pass no matter what
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
