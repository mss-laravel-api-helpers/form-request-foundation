<?php
/**
 * Trait to be applied to request that has no validation rules
 */

namespace Gila\LaravelApiHelpers\FormRequestFoundation\Traits;

/**
 * Trait HasNoRules
 * @package Gila\LaravelApiHelpers\FormRequestFoundation\Traits
 */
trait HasNoRules
{
    /**
     * Returns empty array of validation rules for this request
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }
}
