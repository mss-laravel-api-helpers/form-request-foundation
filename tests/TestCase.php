<?php
/**
 * Base test case
 */

namespace Gila\LaravelApiHelpers\FormRequestFoundation\Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

/**
 * Class TestCase
 * @package Gila\LaravelApiHelpers\RepositoryPatternHelper\Tests
 */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }
}
