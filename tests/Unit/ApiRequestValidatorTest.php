<?php
/**
 * Test for extending AbstractApiRequestValidator
 */

namespace Gila\LaravelApiHelpers\FormRequestFoundation\Tests\Unit;

use Gila\LaravelApiHelpers\FormRequestFoundation\AbstractApiRequestValidator;
use Gila\LaravelApiHelpers\FormRequestFoundation\Tests\TestCase;
use Gila\LaravelApiHelpers\FormRequestFoundation\Traits\DefersAuthorization;
use Gila\LaravelApiHelpers\FormRequestFoundation\Traits\HasNoRules;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class ApiRequestValidatorTest
 * @package Gila\LaravelApiHelpers\FormRequestFoundation\Tests\Unit
 */
class ApiRequestValidatorTest extends TestCase
{
    /**
     * @var AbstractApiRequestValidator
     */
    protected $request;

    public function setUp(): void
    {
        parent::setUp();

        $this->request = new class extends AbstractApiRequestValidator {

            public function getPolicyAction(): string
            {
                return 'policy-action';
            }

            public function getPolicyActor()
            {
                return 'policy-actor';
            }

            public function testSetDataTypeJson()
            {
                $this->setDataType(self::DATATYPE_JSON);
            }

            public function testSetDataTypeForm()
            {
                $this->setDataType(self::DATATYPE_FORM);
            }

            public function testGetValidationData()
            {
                return $this->validationData();
            }

            public function testSetValidationData(array $data)
            {
                $this->setValidationData($data);
            }
        };
    }

    public function testCanSetDataType()
    {
        $this->request->testSetDataTypeJson();
        $this->assertTrue($this->request->expectsJson());

        $this->request->testSetDataTypeForm();
        $this->assertFalse($this->request->expectsJson());
    }

    public function testValidationDataReturnsAll()
    {
        $this->request->testSetDataTypeForm();
        $this->request->merge([
            'key1' => 'value1',
            'key2' => 'value2'
        ]);

        $this->assertSame([
            'key1' => 'value1',
            'key2' => 'value2'
        ], $this->request->testGetValidationData());
    }

    public function testValidationDataReturnsAllAsJson()
    {
        $this->request->testSetDataTypeJson();
        $this->request->setJson(new ParameterBag([
            'key1' => 'value1',
            'key2' => 'value2'
        ]));

        $this->assertSame([
            'key1' => 'value1',
            'key2' => 'value2'
        ], $this->request->testGetValidationData());
    }

    public function testValidationDataReturnsOnlySelectedFieldsAsForm()
    {
        $this->request->testSetDataTypeForm();
        $this->request->testSetValidationData(['key2']);

        $this->request->merge([
            'key1' => 'value1',
            'key2' => 'value2'
        ]);

        $this->assertSame([
            'key2' => 'value2'
        ], $this->request->testGetValidationData());
    }

    public function testValidationDataReturnsOnlySelectedFieldsAsJson()
    {
        $this->request->testSetDataTypeJson();
        $this->request->testSetValidationData(['key2']);

        $this->request->setJson(new ParameterBag([
            'key1' => 'value1',
            'key2' => 'value2'
        ]));

        $this->assertSame([
            'key2' => 'value2'
        ], $this->request->testGetValidationData());
    }

    public function testRequestWithNoRulesReturnsEmptyArray()
    {
        $request = new class extends AbstractApiRequestValidator {
            use HasNoRules;

            public function getPolicyAction(): string
            {
                return 'policy-action';
            }

            public function getPolicyActor()
            {
                return 'policy-actor';
            }
        };

        $this->assertEmpty($request->rules());
    }

    public function testRequestDefersAuthorization()
    {
        $request = new class extends AbstractApiRequestValidator {
            use DefersAuthorization;

            public function getPolicyAction(): string
            {
                return 'policy-action';
            }

            public function getPolicyActor()
            {
                return 'policy-actor';
            }
        };

        $this->assertTrue($request->authorize());
    }
}
