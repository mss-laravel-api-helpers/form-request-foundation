<?php
/**
 * Test for models that build validation rules
 */

namespace Gila\LaravelApiHelpers\FormRequestFoundation\Tests\Unit;

use Gila\LaravelApiHelpers\FormRequestFoundation\Contracts\ValidationRulesContract;
use Gila\LaravelApiHelpers\FormRequestFoundation\Models\Traits\BuildsValidationRules;
use Gila\LaravelApiHelpers\FormRequestFoundation\Tests\TestCase;

/**
 * Class ModelValidationRulesTest
 * @package Gila\LaravelApiHelpers\FormRequestFoundation\Tests\Unit
 */
class ModelValidationRulesTest extends TestCase
{
    public function testModelBuildsValidationRules()
    {
        $model = new class implements ValidationRulesContract {
            use BuildsValidationRules;

            public function rules(): array
            {
                return [
                    ValidationRulesContract::VALIDATE_BASE => [
                        'key1' => ['string'],
                        'key2' => ['integer'],
                        'key3' => ['bool']
                    ],
                    ValidationRulesContract::VALIDATE_CREATE => [
                        'key1' => [ValidationRulesContract::VALIDATION_REQUIRED]
                    ],
                    ValidationRulesContract::VALIDATE_UPDATE => [
                        'key3' => [ValidationRulesContract::VALIDATION_REQUIRED]
                    ]
                ];
            }
        };

        $this->assertSame([
            'key1' => 'string|required',
            'key2' => 'integer',
            'key3' => 'bool'
        ], $model->buildValidationRules(ValidationRulesContract::VALIDATE_CREATE));
        $this->assertSame([
            'key1' => 'string',
            'key2' => 'integer',
            'key3' => 'bool|required'
        ], $model->buildValidationRules(ValidationRulesContract::VALIDATE_UPDATE));
    }

    public function testModelCanBuildRuleFromPipedString()
    {
        $model = new class implements ValidationRulesContract {
            use BuildsValidationRules;

            public function rules(): array
            {
                return [
                    ValidationRulesContract::VALIDATE_BASE => [
                        'key1' => ['string|max:420'],
                        'key2' => ['integer|min:1|max:99'],
                        'key3' => ['bool']
                    ],
                    ValidationRulesContract::VALIDATE_CREATE => [
                        'key1' => [ValidationRulesContract::VALIDATION_REQUIRED]
                    ],
                    ValidationRulesContract::VALIDATE_UPDATE => [
                        'key3' => [ValidationRulesContract::VALIDATION_REQUIRED]
                    ]
                ];
            }
        };

        $this->assertSame([
            'key1' => 'string|max:420|required',
            'key2' => 'integer|min:1|max:99',
            'key3' => 'bool'
        ], $model->buildValidationRules(ValidationRulesContract::VALIDATE_CREATE));
        $this->assertSame([
            'key1' => 'string|max:420',
            'key2' => 'integer|min:1|max:99',
            'key3' => 'bool|required'
        ], $model->buildValidationRules(ValidationRulesContract::VALIDATE_UPDATE));
    }
}
